Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    namespace :api do
        namespace :v1 do
            get  '/resources', to: 'api#index',  as: 'index'
            post '/resources', to: 'api#create', as: 'create'
        end
    end


    root 'gui#index'

    get  '/resource/new(.:format)',      to: 'gui#new',                  as: 'new_resource'
    post '/resource(.:format)',          to: 'resource#create',          as: 'create_resource'
    get  '/resource/override(.:format)', to: 'gui#override',             as: 'new_override'
    post '/resource/override(.:format)', to: 'resource#create_override', as: 'create_override'
end
