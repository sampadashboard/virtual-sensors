class Api::V1::ApiController < ApiController
    def index
        render  json: Resource.all,
                status: :ok
    end

    def create
        @r = Resource.new
        @r.latitude,@r.longitude = params[:lat],params[:lon]
        @r.capabilities = params_capabilities
        @r.description = params[:description]

        if @r.save
            launch_workers unless ENV['RAILS_ENV'] == 'test'
            render  json: {message: 'OK', resource: @r},
                    status: :created
        else
            render  json: {error: @r.errors},
                    status: :unauthorized
        end
    end

    private

        def params_capabilities
            return nil if
                not params.include?(:capabilities) or
                not params[:capabilities].respond_to?(:map)

            params[:capabilities].map{|c| c[:name]}
        end

        def launch_workers
            @r.capabilities.each do |cap|
                CapabilityWorker.perform_async params_hash(cap)
            end
        end

        def params_hash(cap)
            h = Hash.new
            h[:strategy] = cap
            h[:uuid] = @r.uuid
            h[:code] = params[:code]
            h[:send_rate] = params[:send_rate]
            h
        end
end
