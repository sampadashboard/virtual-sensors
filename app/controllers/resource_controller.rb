class ResourceController < ApplicationController

    # this controller must have an way to differentiate
    # API requests from GUI requests
    #
    # Another problematic item is session with API

    def create
        @r = Resource.new
        @r.latitude, @r.longitude =
            params[:coordinates].gsub(/[^0-9\.,;-]/,'').split(/[,;]/) if
                params.include? :coordinates
        @r.capabilities = params[:options] if params.include? :options
        @r.description = params[:description] if params.include? :description

        if @r.valid?
            if params[:override]
                session[:resource] = @r
                redirect_to new_override_path
            else
                @r.save
                flash[:success] = "Successfuly created"
                launch_workers
                redirect_to root_path
            end
        else
            flash[:error] = @r.errors.first.join ' '
            redirect_to new_resource_path(safe_params)
        end
    end

    def create_override
        if override_form_valid?
            sess_to_res
            session.delete(:resource)
            @r.save
            launch_workers
            flash[:success] = "Successfuly created"
            redirect_to root_path
        else
            flash[:error] = "You must input something"
            redirect_to new_override_path
        end
    end

    private
		def sess_to_res
			sr = session[:resource]
			@r = Resource.new
			@r.capabilities = sr['capabilities']
			@r.latitude,@r.longitude = sr['latitude'], sr['longitude']
			@r.description = sr['description']
		end

        def override_form_valid?
            input = ''
            params.keys.select do |k|
                k =~ /_code$/ or k =~ /_amount$/
            end.each{|k| input += params[k]}
            not input.eql? ''
        end

        def launch_workers
            @r.capabilities.each do |cap|
                CapabilityWorker.perform_async params_hash(cap)
            end
        end

        def params_hash(cap)
            h = Hash.new
            h[:strategy] = cap
            h[:uuid] = @r.uuid
            h[:code] = params["#{cap}_code"]
            if params.include? "#{cap}_send_amount"
                s = params["#{cap}_send_amount"]
                s /= 60.0 if params["#{cap}_send_unity"].eql? "Hour"
                h[:send_rate] = s
            end
            h
        end

        def safe_params
            p = {}
            [:options, :description, :coordinates, :ruby_code].
                each { |a| p[a] = params[a] if params.include? a }
            p
        end
end
