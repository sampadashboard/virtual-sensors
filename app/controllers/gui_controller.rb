require "#{Rails.root}/app/workers/capability_worker.rb"
require "#{Rails.root}/lib/capability.rb"

class GuiController < ApplicationController
    def index
        # an idea is to show a list of current capabilities (atm; resources later)
        @resources = Resource.all
    end

    def new
        @capabilities = {
            sound: ['Sound','bullhorn'],
            illumination: ['Illumination','lamp'],
            car_flow: ['Car Flow','road'],
            people_density: ['People Density','user'],
            video: ['Video','facetime-video'],
            weather: ['Weather','cloud'],
            air_quality: ['Air Quality','leaf']
        }
    end

    def override
        sess_to_res
    end

    private
        def sess_to_res
            sr = session[:resource]
            @r = Resource.new
            @r.capabilities = sr['capabilities']
            @r.latitude,@r.longitude = sr['latitude'], sr['longitude']
            @r.description = sr['description']
        end
end
