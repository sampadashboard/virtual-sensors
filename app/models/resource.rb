require 'set'
require 'uri'
require 'net/http'

class Resource < ApplicationRecord
    validates :latitude, presence: true, numericality: {
        less_than_or_equal_to: 90,
        greater_than_or_equal_to: -90 }

    validates :longitude, presence: true, numericality: {
        less_than_or_equal_to: 180,
        greater_than_or_equal_to: -180 }

    validates :description, presence: true, length: { in: 5..255 }

    validate :capabilities_are_valid

    before_save :ensure_registered

    def capabilities=(caps)
        if caps.respond_to? :join
            write_attribute(:capabilities,caps.uniq.join(', '))
        else
            write_attribute(:capabilities,'')
        end

        if registered?
            if capabilities.eql? []
                update "inactive"
            else
                update
            end
        end

    end

    def capabilities
        if read_attribute(:capabilities).respond_to? :split
            read_attribute(:capabilities).split(', ')
        else
            read_attribute(:capabilities)
        end
    end


    def capabilities_are_valid
        all_caps = Set.new(['air quality', 'air_quality', 'car flow', 'car_flow', 'illumination', 'people density', 'people_density', 'sound', 'weather', 'video'])

        my_caps = Set.new(capabilities)

        errors.add(:capabilities,"invalid capabilities") if
            my_caps.size == 0 or
            not my_caps.subset? all_caps
    end

    private
        def register
            base_url = Settings.platform.base_url
            url = URI.parse(base_url+Settings.platform.register_uri)
            req = Net::HTTP::Post.new(url.to_s,{
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'})

            req.body = {
                data:{
                    description: description,
                    capabilities: capabilities,
                    status: "active",
                    lat: latitude,
                    lon: longitude
                }}.to_json

            # http = Net::HTTP.new(url.host,url.port)
            # res = http.request(req)
            #Net::HTTP.post (URI(url.host), req.body, "Content-Type" => "application/json")

            res = Net::HTTP.start(url.host,url.port) {|http|                 
                http.read_timeout = 60
                http.request(req)
            }
            res_body = JSON.parse(res.body) 
            write_attribute(:uuid, res_body["data"]["uuid"]) 
        end

        def registered?
            not read_attribute(:uuid).nil?
        end

        def ensure_registered
            register unless registered?
        end

        def update(status="active")
            base_url = Settings.platform.base_url
            url = URI.parse(update_data_url(uuid))
            req = Net::HTTP::Put.new(url.to_s,{
                'Accept'=>'*/*',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Content-Type'=>'application/json',
                'User-Agent'=>'Ruby'})

            req.body = {
                description: description,
                capabilities: capabilities,
                status: status,
                lat: latitude,
                lon: longitude
            }.to_json

            res = Net::HTTP.start(url.host,url.port) {|http| http.request(req)}
        end

end
