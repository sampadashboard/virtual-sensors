// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require_tree .
//= require bootstrap-sprockets

function createCodeForm() {

    // works only if it's the first time
    if (document.getElementById('ruby_code') == null) {
        // hides the trigger button
        document.getElementById('trigger').style.display = 'none';

        // find key elements
        var form = document.getElementById('form');
        var btn_div = document.getElementById('submit_btn');

        // creates a div as a wrapper
        var div = document.createElement('div');
        div.classList.add('form-group');
        div.classList.add('well');

        // creates two line-breaker
        var b1 = document.createElement('br');
        var b2 = document.createElement('br');

        // places the div-wrapper where it's meant
        form.insertBefore(div,btn_div);
        form.insertBefore(b1,btn_div);
        form.insertBefore(b2,btn_div);

        // creates the elements that will go inside the div-wrapper
        // a textarea
        var textarea = document.createElement("textarea");
        textarea.id = 'ruby_code';
        textarea.name = "ruby_code";
        textarea.rows = 10;
        textarea.cols = 80;
        textarea.classList.add('code-block');
        textarea.innerHTML = "# your ruby code goes here\n";
        // a label
        var label = document.createElement('label');
        label.innerHTML = "Your code:";
        // a line-breaker
        var br = document.createElement('br');

        // places last 3 elements inside the div-wrapper in order
        div.appendChild(label);
        div.appendChild(br);
        div.appendChild(textarea);
    }
}
