require "#{Rails.root}/lib/capability.rb"
require "#{Rails.root}/lib/peopledensity.rb"
require "#{Rails.root}/lib/airquality.rb"
require "#{Rails.root}/lib/illumination.rb"
require "#{Rails.root}/lib/carflow.rb"
require "#{Rails.root}/lib/sound.rb"
require "#{Rails.root}/lib/video.rb"
require "#{Rails.root}/lib/weather.rb"

class CapabilityWorker
    include Sidekiq::Worker

    def perform(params_hash)
        strategy = params_hash["strategy"]
        cap = self.class.const_get(strategy.split('_').map{|s|s.capitalize}.join).new(params_hash)
		cap.death_prob = params_hash[:death_prob] if params_hash.include? :death_prob
        cap.play params_hash[:code]
    end

end
