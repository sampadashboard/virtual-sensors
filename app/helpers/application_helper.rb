module ApplicationHelper
    def flash_bootstrap_class(level)
        case level.to_sym
        when :notice then "alert alert-info"
        when :success then "alert alert-success"
        when :error then "alert alert-danger"
        when :alert then "alert alert-warning"
        end
    end

    def send_data_url(uuid,capability)
        u = Settings.platform.send_data_uri.
            gsub(/\:uuid/,uuid).
            gsub(/\:capability/,capability)

        Settings.platform.base_url + u
    end

    def update_data_url(uuid)
        u = Settings.platform.update_uri.
            gsub(/\:uuid/,uuid)

        Settings.platform.base_url + u
    end
end
