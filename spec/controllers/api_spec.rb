require 'json'

describe Api::V1::ApiController, type: :controller do
    it 'returns HTTP success when GET /api/v1/resources' do
        get 'index'

        expect(response).to have_http_status :success
    end

    it 'returns HTTP created when POST /api/v1/resources with correct parameters' do
        resource = {
            lat: -23.558767,
            lon: -46.731501,
            capabilities: [
                { name: "illumination" },
                { name: "weather" }
            ],
            description: "This is a resource created during a Rspec test"
        }

        post 'create', params: resource

        expect(response).to have_http_status :created
    end

    it 'returns HTTP unauthorized when POST /api/v1/resources with incorrect parameters' do
        resource = {
            description: "This resource will never be created"
        }

        post 'create', params: resource

        expect(response).to have_http_status :unauthorized
    end
end
