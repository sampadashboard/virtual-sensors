require_relative '../../lib/capability.rb'
require_relative '../../lib/airquality.rb'
require_relative '../../lib/peopledensity.rb'
require_relative '../../lib/carflow.rb'
require_relative '../../lib/sound.rb'
require_relative '../../app/workers/capability_worker.rb'

require 'sidekiq/testing'
require 'securerandom'

describe 'Enqueueing only' do
    it 'enqueues properly a capabilitiy with default behavior' do
        Sidekiq::Testing.fake!
        expect {
            CapabilityWorker.perform_async "people_density", "UFFA-2213"
        }.to change(CapabilityWorker.jobs, :size).by 1
    end

    it 'enqueues properly a capabilitiy with overriden behavior' do
        Sidekiq::Testing.fake!
        expect {
            code = "if precise?
                @data = 10 * rand
            else
                @data = -1.0
            end
            @time = Time.now"
            CapabilityWorker.perform_async "people_density", "UFFA-2213", code
        }.to change(CapabilityWorker.jobs, :size).by 1
    end
end

describe 'Trully working' do
    it 'raises an exception when try to run without arguments' do
        Sidekiq::Testing.inline! do
            expect {
                CapabilityWorker.perform_async
            }.to raise_error ArgumentError
        end
    end

    [
		:sound,:weather,:air_quality,:car_flow,:people_density,:video,:illumination
	].each do |cap|
        it "does not raise an exception when arguments are correctly passed to #{cap}" do
            r = Resource.new
            r.capabilities = [cap.to_s]
            r.latitude, r.longitude = 1.4, 1.4
            r.description = 'a test resource'
            r.uuid = SecureRandom.uuid

			params_hash = {
				"strategy" => cap.to_s,
				code: nil,
				uuid: r.uuid,
				death_prob: 1
			}

            Sidekiq::Testing.inline! do
                expect {
                    CapabilityWorker.perform_async params_hash
                }.to_not raise_error
            end
        end
    end
end
