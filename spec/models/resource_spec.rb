require 'rails_helper'

RSpec.describe Resource, type: :model do
    it 'does not allow a save without coordinates' do
        r = Resource.new
        r.capabilities = ['car_flow','temperature']
        r.description = "a resource"
        expect(r.valid?).to be_falsey
    end

    it 'does not allow a save without capabilities' do
        r = Resource.new
        r.latitude = 1.2
        r.longitude = 1.4
        r.description = "a resource"
        expect(r.valid?).to be_falsey
    end

    it 'does not allow to save with coordinates out of range' do
        r = Resource.new
        r.latitude = -12930
        r.longitude = 0
        r.capabilities = ['car_flow']
        r.description = "a resource"
        expect(r.valid?).to be_falsey
    end

    it 'does not allow saving without description' do
        r = Resource.new
        r.latitude = 1.2
        r.longitude = 1.4
        r.capabilities = ['car_flow']
        expect(r.valid?).to be_falsey
    end

    it 'saves successfuly when coordinates, description and capabilities are given' do
        r = Resource.new
        r.latitude = 1.2
        r.longitude = 1.4
        r.capabilities = ['car_flow','weather']
        r.description = "a resource"
        expect(r.valid?).to be_truthy
    end

    it 'parses correctly the capabilities' do
        r = Resource.new
        caps = ['car_flow','temperature']
        r.capabilities = caps
        expect(r.capabilities).to eq(caps)
    end

    it 'registers properly with the platform and gets a UUID' do
        r = Resource.new
        r.latitude = 1.2
        r.longitude = 1.4
        r.capabilities = ['car_flow','weather']
        r.description = "a resource"
        r.save
        expect(r.uuid).to_not be_nil
    end

end
