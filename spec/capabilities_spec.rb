require_relative '../lib/capability.rb'
require_relative '../lib/airquality.rb'
require_relative '../lib/peopledensity.rb'
require_relative '../lib/carflow.rb'
require_relative '../lib/sound.rb'
require_relative '../lib/illumination.rb'
require_relative '../lib/video.rb'
require_relative '../lib/weather.rb'

require 'securerandom'

RSpec::Matchers.define_negated_matcher :be_not, :be

describe AirQuality do

	before :each do
		params_hash = {uuid: SecureRandom.uuid}
		@aq = AirQuality.new params_hash
		@aq.generate_data
	end

	it 'generates data' do
		expect(@aq.data).to be_not(nil).
			and include(:polluting_index).
			and include(:polluting)
	end

	it 'generated data is what was defined' do
		expect(@aq.data[:polluting_index]).to be_between(
				AirQuality::Index.min,
				AirQuality::Index.max + 1
			).inclusive.
			or eq(-1)
	end

end

describe CarFlow do

	before :each do
		params_hash = {uuid: SecureRandom.uuid}
		@cf = CarFlow.new params_hash
		@cf.generate_data
	end

	it 'generates data' do
		expect(@cf.data).to be_not(nil).
			and include(:car_flow).
			and include(:car_flow_class)
	end

	it 'generated data is what was defined' do
		expect(@cf.data[:car_flow]).to be_between(0.0, 1.0).inclusive.
			or eq(-1)
	end

end

describe Illumination do

	before :each do
		params_hash = {uuid: SecureRandom.uuid}
		@il = Illumination.new params_hash
		@il.generate_data
	end

	it 'generates data' do
		expect(@il.data).to be_not(nil).
			and include(:illuminance).
			and include(:illuminance_class)
	end

	it 'generated data is what was defined' do
		expect(@il.data[:illuminance]).to be_between(
				Illumination::Lux.min,
				Illumination::Lux.max + 1
			).inclusive.
			or eq(-1)
	end

end

describe PeopleDensity do

	before :each do
		params_hash = {uuid: SecureRandom.uuid}
		@pd = PeopleDensity.new params_hash
		@pd.generate_data
	end

	it 'generates data' do
		expect(@pd.data).to be_not(nil).
			and include(:people_density).
			and include(:date)
	end

	it 'generated data is what was defined' do
		expect(@pd.data[:people_density]).to be_between(0,6).inclusive.
			or eq(-1)
	end

end

describe Sound do

	before :each do
		params_hash = {uuid: SecureRandom.uuid}
		@sd = Sound.new params_hash
		@sd.generate_data
	end

	it 'generates data' do
		expect(@sd.data).to be_not(nil).
			and include(:sound).
			and include(:date)
	end

	it 'generated data is what was defined' do
		expect(@sd.data[:sound]).to be_between(0,110).inclusive.
			or eq(-1)
	end

end

describe Video do

	before :each do
		params_hash = {uuid: SecureRandom.uuid}
        @vd = Video.new params_hash
        @vd.generate_data
	end

	it 'generates data' do
		expect(@vd.data).to be_not(nil).
			and include(:video).
			and include(:date)
	end

	it 'generated data is what was defined' do
        File.open('image.jpg', 'wb') do |f|
            f.write(Base64.decode64(@vd.data[:video]))
        end
        result = @vd.image?('image.jpg')
        File.delete('image.jpg')
        expect(result).to be true
	end

end

describe Weather do

	before :each do
		params_hash = {uuid: SecureRandom.uuid}
		@wt = Weather.new params_hash
		@wt.generate_data
	end

	it 'generates data' do
		expect(@wt.data).to be_not(nil).
			and include(:temperature).
			and include(:humidity).
			and include(:wind_speed).
			and include(:cloud_cover)
	end

	it 'generated temperature is what was defined' do
		expect(@wt.data[:temperature]).to be_between(
				Weather::Celsius.min,
				Weather::Celsius.max + 1
			).inclusive.
			or eq(-1)
	end

	it 'generated humidity is what was defined' do
		expect(@wt.data[:humidity]).to be_between(0,1).inclusive.
			or eq(-1)
	end

	it 'generated wind speed is what was defined' do
		expect(@wt.data[:wind_speed]).to be_between(
				Weather::Wind_Speed.min,
				Weather::Wind_Speed.max + 1
			).inclusive.
			or eq(-1)
	end

	it 'generated cloud cover is what was defined' do
		expect(@wt.data[:cloud_cover]).to be_between(0,100).inclusive.
			or eq(-1)
	end

end
