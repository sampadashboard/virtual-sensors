class PeopleDensity < Capability
    def generate_data
		@time = Time.now
        if precise?
            @data = {people_density: 6 * rand, date: @time}
        else
            @data = {people_density: -1.0, date: @time}
        end
    end
end
