include ApplicationHelper
require 'net/http'
require 'json'
require 'safe_ruby'

class Capability

    attr_reader :data, :send_rate
    attr_writer :death_prob

    def initialize(params_hash)
        @precision = 1.0 - 1e-4
        @death_prob = params_hash.include?("death_prob") ?
			params_hash["death_prob"] : 1e-6
        @res_uuid = params_hash["uuid"]
        @send_rate = (params_hash[:send_rate] or 1) # in minutes
    end

    def play(code)
        loop do
            if code.nil?
                generate_data
            else
                run code
            end
            break if dead?
			send_data
            sleep(60.0*@send_rate)
        end
        remove_from_resource unless @death_prob.eql? 1
    end

    def run(code)
        @data = SafeRuby.eval(code)
        @time = Time.now
    end

    def precise?
        rand < @precision
    end

    def dead?
        rand <= @death_prob
    end

    def send_data
        base_url = Settings.platform.base_url
        url = URI.parse(send_data_url(@res_uuid,self.class.to_s.downcase))
        req = Net::HTTP::Post.new(url.to_s,{'Accept'=>'*/*',
                'Content-Type'=>'application/json',
                'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'User-Agent'=>'Ruby'})

        req.body = {
            data: [{
                value: @data
            }]}.to_json

        res = Net::HTTP.start(url.host,url.port) {|http| http.request(req)}
        return res.code
    end

    def remove_from_resource
        resource = Resource.find_by(uuid: @res_uuid)
        caps = resource.capabilities
        caps.delete(self.class.to_s.
                    split(/(?=[A-Z])/).map{|i| i.downcase}.join(' '))
        resource.capabilities = caps
        resource.save
    end

end
