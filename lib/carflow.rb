class CarFlow < Capability

    CLASSES = {
        (0...(1.0/3))       => "Heavy jam",
        ((1.0/3)...(2.0/3)) => "Jam",
        ((2.0/3)..1)        => "Clear"
    }

    def generate_data
        if precise?
            @data = data_compiled
        else
            @data = {car_flow: -1, car_flow_class: "Regular"}
        end
        @time = Time.now
    end

    private
        def data_compiled
            curr_f = current_flow
            f_cls  = flow_class curr_f
            return {car_flow: curr_f, car_flow_class: f_cls, date: @data}
        end

        def flow_class(f)
            CarFlow::CLASSES.each do |k,v|
                return v if k.include? f
            end
        end

        def current_flow
            t = Time.now.sec/60.0
            0.5 * (1 + Math.sin(2 * t * Math::PI))
        end
end
