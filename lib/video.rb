require "base64"

class Video < Capability

  def generate_data
    if precise?
      @filename = '1.jpg'
    else
      @filename = 'error.jpg'
    end
    file_content = File.open(File.expand_path('../images/' + @filename, __FILE__)) { |f| f.read }
    @data = {video: Base64.encode64(file_content), date: @time}

    @time = Time.now
  end

  # check if the file is a jpeg image, used only by rspec tests
  def image?(filename)
    f = File.open(filename,'rb')  # rb means to read using binary
    data = f.read(9)              # magic numbers are up to 9 bytes
    f.close
    return data[0,4] == "\xff\xd8\xff\xe0".b #compare data header with jpeg header
  end
end
