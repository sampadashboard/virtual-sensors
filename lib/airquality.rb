class AirQuality < Capability

    Index = (0...500)
    CLASSES = {
        (0..50)     => "Good",
        (51..100)   => "Tolerable",
        (101...200) => "Inadequate",
        (200...300) => "Unhealthy",
        (300...400) => "Harmful",
        (400...500) => "Very harmful"
    }

    def generate_data
        if precise?
            @data = data_compiled
        else
            @data = {polluting_index: -1, polluting: "Error"}
        end
        @time = Time.now
    end

    private
        def data_compiled
            curr_q = current_quality
            q_cls  = quality_class curr_q
            return {polluting_index: curr_q, polluting: nil, air_quality: q_cls, date: @time}
        end
        def quality_class(q)
            AirQuality::CLASSES.each do |k,v|
                return v if k.include? q
            end
        end

        def current_quality
            h = Time.now.min/60.0
            Index.min +
                Index.size * 0.5 * (1 + Math.cos(Math::PI * h))
        end
end
