class Weather < Capability

    Celsius = (10..35)
    Wind_Speed = (0..30)

    def generate_data
        if precise?
            @data = data_compiled
        else
            @data = {
				temperature: -1,
				humidity: -1,
				wind_speed: -1,
				cloud_cover: -1,
				date: Time.now
			}
        end
    end

    private
        def data_compiled
            t = Time.now.hour/24.0
            tprt = Weather::Celsius.min +
                (Weather::Celsius.size-1) * Math.sin(Math::PI * t)
            humdt = Math.sin(t * Math::PI)
            wnd_spd = Weather::Wind_Speed.min +
                (Weather::Wind_Speed.size-1) * Math.sin(Math::PI * t)
            cld_cvr = rand(100)
            return {temperature: tprt, humidity: humdt, wind_speed: wnd_spd, cloud_cover: cld_cvr, date: @time}
        end
end
