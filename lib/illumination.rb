class Illumination < Capability

    Lux = (5..750)
    CLASSES = {
        (5...50)    => "Very low",
        (50...100)  => "Low",
        (100...300) => "Medium",
        (300...500) => "High",
        (500...750) => "Very high"
    }

    def generate_data
        if precise?
            @data = data_compiled
        else
            @data = {illuminance: -1, illuminance_class: "Error"}
        end
        @time = Time.now
    end

    private
        def data_compiled
            curr_il = current_illuminance
            il_cls  = illuminance_class curr_il
            return {illuminance: curr_il, illuminance_class: il_cls, date: @time }
        end

        def illuminance_class(il)
            Illumination::CLASSES.each do |k,v|
                return v if k.include? il
            end
        end

        def current_illuminance
            h = Time.now.hour + Time.now.min/60.0
            Illumination::Lux.min +
                (Illumination::Lux.size-1) * Math.sin(Math::PI * h / 24.0)
        end
end
