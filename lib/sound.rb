class Sound < Capability
  def generate_data
	@time = Time.now
    if precise?
      @data = {sound: rand*110, date: @time}
    else
      @data = {sound: -1, date: @time}
    end
  end
end
