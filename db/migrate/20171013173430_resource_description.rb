class ResourceDescription < ActiveRecord::Migration[5.1]
    def up
        change_table :resources do |t|
            t.string :description
        end
        Resource.update_all ['description = ?', 'a resource']
    end

    def down
        remove_colum :resource, :description
    end
end
