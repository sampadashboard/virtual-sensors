class CreateResources < ActiveRecord::Migration[5.1]
    def change
        create_table :resources do |t|
            t.string :uuid
            t.decimal :latitude
            t.decimal :longitude
            t.text :capabilities
            t.timestamps
        end
    end
end
