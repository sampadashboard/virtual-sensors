# SensoreSampa

Nosso grupo é encarregado da criação de um sistema que forneça virtualizações de recursos para a plataforma SampaDashboard. Recursos são objetos do mundo real e a modelagem da plataforma é feita tal que um recurso pode ter capacidades e ser um atuador.
A título de exemplo, pense num **ponto de ônibus** como um recuso: ele pode ter a _capacidade_ de informar à plataforma a qualidade do ar, temperatura e umidade onde ele está, e pode ser um _atuador_ exibindo à população local o tempo estimado para a chegada de um determinado ônibus. Outro exemplo seria o recurso **semáforo** que pode ter a _capacidade_ de informar à plataforma a situação do trânsito local, ao mesmo tempo que pode ser um _atuador_ alterando os tempos de cada estado para favorecer o fluxo de carros.
As _capacidades_ que daremos aos nossos recursos virtuais são:
- **umidade** do ar
- **temperatura** ambiente
- **qualidade do ar**
- **vídeo**: provê uma visualização do local em tempo real, com imagens de câmeras
- **sonoro**: informa sobre as condições de silêncio e poluição sonora
- **fluxo** de carros
- densidade de **pessoas**
- **iluminação**


## Principais Funcionalidades
1. implementar uma variedade de recursos virtuais
2. implementar uma fábrica de recursos capaz de gerar os diversos tipos de recursos virtuais
3. permitir uma especificação do usuário para a determinação das informações geradas pelos sensores (ex: determinar uma distribuição para a geração de temperaturas ou o _fps_ das imagens de vídeo)
4. criar uma interface gráfica que permita a fácil seleção de recurso e sua localização em um mapa
5. criar uma interface de programação de aplicativos (API) que permita a integração do nosso projeto a outros aplicativos


## Instalar e rodar
> É necessário que haja uma instalação de Redis Server rodando, assim como de PostgreSQL com os bancos de dados "virtual\_sensors\_development", "virtual\_sensors\_test" e "virtual\_sensors\_production" criados, e com as variáveis USERNAME e PASSWORD devidamente definidas no sistema

- para rodar no modo desenvolvimento, no diretório raiz do projeto, diga `./build_and_run`
- para rodar os testes, no diretório raiz do projeto, diga `./run_tests` 

## Grupo

- Fábio Tanaka (@fhtanaka | fhktanaka@gmail.com)
- João Daniel (@jotaf.daniel | jotaf.daniel@gmail.com)
- Lucas Abe (@stefanlucas | lucastefan9@gmail.com)
- Luiz Girotto (@LuizGyro | luizfelipe.g@hotmail.com)
- Vítor Tamada (@vitorkei | vitor\_kei9@hotmail.com)

